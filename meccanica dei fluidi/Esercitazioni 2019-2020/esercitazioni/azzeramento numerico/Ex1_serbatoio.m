clc
clear all

global rho mi M h H D K90 g 

%% Dati
rho = 870; %[kg/m3]
mi = 2*10^-2; %[Pa*s]
M = 3; %[m] 
h = 1.5; %[m]
H = 3; %[m]
D = 0.03; %[m]
K90 = 0.4;
g = 9.81; %[m/s2]

%% Azzeramento
u0 = (2*g*H)^0.5; %[m/s] Primo tentativo
fun = @Bernoulli; %Inizializzazione funzione
u = fsolve(fun,u0) %Risolutore

%% Calcolo portata e verifica Re
Re = rho*u*D/mi %Verifica Reynolds turbolento (> 2100)
Q = u*3.14*D^2/4 %[m3/s]

